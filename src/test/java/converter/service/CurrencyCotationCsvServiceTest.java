package converter.service;

import converter.dao.CurrencyConverterDao;
import converter.model.CurrencyConversion;
import converter.model.CurrencyCotation;
import converter.model.CurrencyCotation.Type;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CurrencyCotationCsvServiceTest {

    @Mock
    private CurrencyConverterDao daoMock;

    private CurrencyCotationCsvService service;

    @Before
    public void setUp() {
        service = new CurrencyCotationCsvService(daoMock);
    }

    @Test(expected = RuntimeException.class)
    public void currencyQuotation_exception() {
        Collection<CurrencyCotation> listMocked = new ArrayList<>();

        CurrencyConversion currencyConversionMocked = new CurrencyConversion();
        currencyConversionMocked.setFrom("from");
        currencyConversionMocked.setTo("to");
        currencyConversionMocked.setValue(1D);
        currencyConversionMocked.setQuotation(LocalDate.now());

        when(daoMock.getCotationsOfDay(any(LocalDate.class))).thenReturn(listMocked);

        service.currencyQuotation(currencyConversionMocked);
    }

    @Test
    public void currencyQuotation_conversion_ok() {
        Collection<CurrencyCotation> listMocked = new ArrayList<>();
        listMocked.add(new CurrencyCotation("220", Type.A, "USD", 3.8562, 3.8568, 1.0000, 1.0000));
        listMocked.add(new CurrencyCotation("978", Type.B, "EUR", 4.3891, 4.3914, 1.1382, 1.1386));

        CurrencyConversion currencyConversionMocked = new CurrencyConversion();
        currencyConversionMocked.setFrom("USD");
        currencyConversionMocked.setTo("EUR");
        currencyConversionMocked.setValue(100.00D);
        currencyConversionMocked.setQuotation(LocalDate.now());

        when(daoMock.getCotationsOfDay(any(LocalDate.class))).thenReturn(listMocked);

        BigDecimal result = service.currencyQuotation(currencyConversionMocked);

        Assert.assertTrue(result.compareTo(BigDecimal.ZERO) > 0);
    }
}