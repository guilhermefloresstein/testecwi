package converter;

import org.junit.Test;

public class CurrencyConverterTest {

    @Test
    public void main() {
        CurrencyConverter.main(new String[] {"USD", "EUR", "100.00", "29/11/2018"});
    }

    @Test(expected = RuntimeException.class)
    public void main_exceptionFrom() {
        CurrencyConverter.main(new String[] {"USDAAA", "EUR", "100.00", "29/11/2018"});
    }

    @Test(expected = RuntimeException.class)
    public void main_exceptionParamter() {
        CurrencyConverter.main(new String[] {"EUR", "100.00", "29/11/2018"});
    }

    @Test(expected = RuntimeException.class)
    public void main_exceptionSmallThanZero() {
        CurrencyConverter.main(new String[] {"USD", "EUR", "-100.00", "29/11/2018"});
    }

}