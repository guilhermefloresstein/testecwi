package converter.service;

import converter.dao.ICurrencyConverterDao;
import converter.model.CurrencyConversion;
import converter.model.CurrencyCotation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Collection;
import java.util.logging.Logger;

public class CurrencyCotationCsvService extends AbstractService implements ICurrencyCotationService{

    private final Logger logger = Logger.getLogger(getClass().getSimpleName());

    public CurrencyCotationCsvService(ICurrencyConverterDao dao) {
        super(dao);
    }

    @Override
    public BigDecimal currencyQuotation(CurrencyConversion cotation) {

        final Collection<CurrencyCotation> cotationsOfDay = dao.getCotationsOfDay(checkWeekend(cotation.getQuotation()));
        Double brValue;

        if (!cotation.getFrom().equals(brCoin)) {
            final CurrencyCotation from = cotationsOfDay.stream()
                    .filter(cotOfDay -> cotOfDay.getCoin().equals(cotation.getFrom())).findFirst()
                    .orElseThrow(() -> new RuntimeException("Nenhuma cotação para a moeda " + cotation.getFrom() + " foi encontrada!"));

            brValue = cotation.getValue() * from.getTxBuy();
        } else {
            brValue = cotation.getValue();
        }

        if (!cotation.getTo().equals(brCoin)) {
            final CurrencyCotation to = cotationsOfDay.stream()
                    .filter(cotOfDay -> cotOfDay.getCoin().equals(cotation.getTo())).findFirst()
                    .orElseThrow(() -> new RuntimeException("Nenhuma cotação para a moeda " + cotation.getTo() + " foi encontrada!"));

            return BigDecimal.valueOf(brValue / to.getTxBuy()).setScale(2, RoundingMode.CEILING);
        }

        return new BigDecimal(brValue).setScale(2, RoundingMode.CEILING);
    }

    private LocalDate checkWeekend(LocalDate date) {

        if (date.getDayOfWeek() == DayOfWeek.SATURDAY || date.getDayOfWeek() == DayOfWeek.SUNDAY) {
            date = date.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY));
            logger.info("Date is weekend, using " + date);
        }

        return date;
    }

}
