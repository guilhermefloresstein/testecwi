package converter.service;

import converter.dao.ICurrencyConverterDao;

public abstract class AbstractService<T extends ICurrencyConverterDao> implements IService {

    protected T dao;

    public AbstractService(T dao) {
        this.dao = dao;
    }

    public T getDao() {
        return dao;
    }

    public void setDao(T dao) {
        this.dao = dao;
    }
}
