package converter.service;

import converter.model.CurrencyConversion;

import java.math.BigDecimal;

public interface ICurrencyCotationService extends IService {

    String brCoin = "BRL";

    BigDecimal currencyQuotation(CurrencyConversion cotation);

}
