package converter.model;

public class CurrencyCotation {

    private String cod;
    private Type type;
    private String coin;
    private Double txBuy;
    private Double txSell;
    private Double parityBuy;
    private Double paritySell;

    public CurrencyCotation() {
    }

    public CurrencyCotation(String cod, Type type, String coin, Double txBuy, Double txSell, Double parityBuy, Double paritySell) {
        this.cod = cod;
        this.type = type;
        this.coin = coin;
        this.txBuy = txBuy;
        this.txSell = txSell;
        this.parityBuy = parityBuy;
        this.paritySell = paritySell;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getCoin() {
        return coin;
    }

    public void setCoin(String coin) {
        this.coin = coin;
    }

    public Double getTxBuy() {
        return txBuy;
    }

    public void setTxBuy(Double txBuy) {
        this.txBuy = txBuy;
    }

    public Double getTxSell() {
        return txSell;
    }

    public void setTxSell(Double txSell) {
        this.txSell = txSell;
    }

    public Double getParityBuy() {
        return parityBuy;
    }

    public void setParityBuy(Double parityBuy) {
        this.parityBuy = parityBuy;
    }

    public Double getParitySell() {
        return paritySell;
    }

    public void setParitySell(Double paritySell) {
        this.paritySell = paritySell;
    }

    public enum Type {
        A, B
    }

}
