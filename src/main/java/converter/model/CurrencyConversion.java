package converter.model;

import java.time.LocalDate;

public class CurrencyConversion {

    private String from;
    private String to;
    private Double value;
    private LocalDate quotation;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public LocalDate getQuotation() {
        return quotation;
    }

    public void setQuotation(LocalDate quotation) {
        this.quotation = quotation;
    }
}
