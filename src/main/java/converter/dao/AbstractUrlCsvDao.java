package converter.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public abstract class AbstractUrlCsvDao implements IDao {

    protected Properties properties;

    public AbstractUrlCsvDao() {
        loadProperties();
    }

    private void loadProperties() {
        properties = new Properties();
        try {
            properties.load(new FileInputStream("src/main/resources/config.properties"));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Arquivo de configurações não encontrado");
        }
    }
}
