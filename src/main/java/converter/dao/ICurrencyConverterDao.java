package converter.dao;

import converter.model.CurrencyCotation;

import java.time.LocalDate;
import java.util.Collection;

public interface ICurrencyConverterDao extends IDao {

    Collection<CurrencyCotation> getCotationsOfDay(LocalDate date);

}
