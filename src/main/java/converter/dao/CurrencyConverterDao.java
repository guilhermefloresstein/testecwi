package converter.dao;

import converter.model.CurrencyCotation;
import converter.model.CurrencyCotation.Type;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

public class CurrencyConverterDao extends AbstractUrlCsvDao implements ICurrencyConverterDao {

    public Collection<CurrencyCotation> getCotationsOfDay(LocalDate date) {

        Collection<CurrencyCotation> cotations = new ArrayList<>();
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMaximumFractionDigits(10);

        try {
            final String path = properties.getProperty("db.url") + date.format(DateTimeFormatter.ofPattern("yyyyMMdd")) + ".csv";
            URL url = new URL(path);
            URLConnection connection = url.openConnection();

            System.out.println("\n Buscando os dados...");
            InputStreamReader input = new InputStreamReader(connection.getInputStream());
            String line;
            String csvSplitBy = properties.getProperty("csv.split");
            BufferedReader buffer = new BufferedReader(input);
            while ((line = buffer.readLine()) != null) {
                String[] col = line.split(csvSplitBy);
                AtomicInteger i = new AtomicInteger(1);

                CurrencyCotation cotation = new CurrencyCotation();
                cotation.setCod(col[i.getAndIncrement()]);
                cotation.setType(Type.valueOf(col[i.getAndIncrement()]));
                cotation.setCoin(col[i.getAndIncrement()]);
                cotation.setTxBuy(numberFormat.parse(col[i.getAndIncrement()]).doubleValue());
                cotation.setTxSell(numberFormat.parse(col[i.getAndIncrement()]).doubleValue());
                cotation.setParityBuy(numberFormat.parse(col[i.getAndIncrement()]).doubleValue());
                cotation.setParitySell(numberFormat.parse(col[i.get()]).doubleValue());

                cotations.add(cotation);
            }
            buffer.close();

        } catch (UnknownHostException e) {
            e.printStackTrace();
            throw new RuntimeException("Sem conexão com a base de dados");
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Nenhuma Cotação encontrada para o dia informado");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return cotations;
    }

}
