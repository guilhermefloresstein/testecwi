package converter;

import converter.dao.CurrencyConverterDao;
import converter.model.CurrencyConversion;
import converter.service.CurrencyCotationCsvService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Optional;

public class CurrencyConverter {

    private static final String PATTERN = "dd/MM/yyyy";

    public static void main(String[] arguments) {
        final Optional<String[]> maybeArgs = Optional.ofNullable(arguments);

        final BigDecimal result =
                maybeArgs
                        .filter(args -> args.length == 4)
                        .filter(args -> Arrays.stream(args).anyMatch(s -> !s.isEmpty()))
                        .filter(args -> args[3].matches("\\d{2}/\\d{2}/\\d{4}"))
                        .filter(args -> Double.parseDouble(args[2]) > 0)
                        .flatMap(args -> {
                            final CurrencyConversion conversion = new CurrencyConversion();
                            conversion.setFrom(args[0]);
                            conversion.setTo(args[1]);
                            conversion.setValue(Double.parseDouble(args[2]));
                            conversion.setQuotation(LocalDate.parse(args[3], DateTimeFormatter.ofPattern(PATTERN)));

                            //Service e dao poderiam ser singletons...
                            final CurrencyCotationCsvService service = new CurrencyCotationCsvService(new CurrencyConverterDao());

                            return Optional.ofNullable(service.currencyQuotation(conversion));
                        })
                        .orElseThrow(() -> new IllegalArgumentException("Argumentos inválidos"));

        String msg = "\nO valor: " + arguments[0] + " " + arguments[2] + " equivale a " +
                arguments[1] + " " + result;
        System.out.println(msg);
    }
}
